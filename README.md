## About The Project

The Email REST API is a microservice that enables you to send mail messages to email addresses stored in a PostgreSQL database. You can also perform CRUD operations on these stored email addresses.

## Endpoints
You can access the service's API documentation by navigating to the following URL after running the service:
  ```
  http://localhost:8080/swagger-ui/index.html
  ```

## Tech stack

* Spring Boot
* Spring Data JPA
* Junit 5, Mockito
* Maven
* PostgreSQL, Docker
* Logback