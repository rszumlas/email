package com.rszumlas.email.exception;

import com.rszumlas.email.entity.Email;
import com.rszumlas.email.repository.EmailResourceRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EmailExceptionHandlerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EmailResourceRepository emailResourceRepository;

    @Test
    public void handleEmailNotFoundException_returnsNotFoundStatusCode() throws Exception {
        mockMvc.perform(get("/api/v1/email-resources/100"))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status", is("NOT_FOUND")))
                .andExpect(jsonPath("$.message", is("Email with id 100 not found")));
    }

    @Test
    public void handleEmailAlreadyExistsException_returnsConflictStatusCode() throws Exception {
        String address = "test@example.com";
        given(emailResourceRepository.findByAddress(address)).willReturn(Optional.of(new Email(1L, address)));
        mockMvc.perform(post("/api/v1/email-resources/test@example.com"))
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.status", is("CONFLICT")))
                .andExpect(jsonPath("$.message", is("Email test@example.com already exists")));
    }

}