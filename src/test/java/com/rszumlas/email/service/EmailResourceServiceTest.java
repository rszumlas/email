package com.rszumlas.email.service;

import com.rszumlas.email.entity.Email;
import com.rszumlas.email.exception.pojo.EmailAlreadyExistsException;
import com.rszumlas.email.exception.pojo.EmailNotFoundException;
import com.rszumlas.email.repository.EmailResourceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

public class EmailResourceServiceTest {

    private EmailResourceService underTest;

    @Mock
    private EmailResourceRepository emailResourceRepository;

    @Captor
    private ArgumentCaptor<Email> emailArgumentCaptor;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        underTest = new EmailResourceService(emailResourceRepository);
    }

    @Test
    public void shouldFindEmail() {
        Email email = new Email("test@test.com");
        when(emailResourceRepository.findById(1L)).thenReturn(Optional.of(email));

        Email foundEmail = underTest.findEmailOrThrow(1L);

        assertEquals(email, foundEmail);
    }

    @Test
    public void shouldNotFindEmailAndThrow() {
        when(emailResourceRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(EmailNotFoundException.class, () -> underTest.findEmailOrThrow(1L));
    }

    @Test
    public void shouldSaveEmail() {
        String address = "tEsT@test.com";
        Email expectedEmail = new Email(address.toLowerCase());
        when(emailResourceRepository.findByAddress(address)).thenReturn(Optional.empty());
        underTest.saveEmail(address);

        then(emailResourceRepository).should().save(emailArgumentCaptor.capture());
        Email actualEmail = emailArgumentCaptor.getValue();

        assertThat(actualEmail)
                .usingRecursiveComparison()
                .isEqualTo(expectedEmail);
    }

    @Test
    public void shouldSaveEmailThrowsException() {
        String address = "test@test.com";
        Email email = new Email(address);
        when(emailResourceRepository.findByAddress(address)).thenReturn(Optional.of(email));

        assertThrows(EmailAlreadyExistsException.class, () -> underTest.saveEmail(address));
    }

    @Test
    public void shouldUpdateEmail() {
        Long id = 1L;
        String newAddress = "new@test.com";
        Email oldEmail = new Email(id, "old@test.com");
        when(emailResourceRepository.findById(id)).thenReturn(Optional.of(oldEmail));

        underTest.updateEmail(id, newAddress);
        Email newEmail = emailResourceRepository.findById(id).get();
        assertEquals(newAddress, newEmail.getAddress());
    }

    @Test
    public void shouldDeleteEmail() {
        Email email = new Email("test@test.com");
        when(emailResourceRepository.findById(1L)).thenReturn(Optional.of(email));

        underTest.deleteEmail(1L);

        verify(emailResourceRepository, times(1)).deleteById(1L);
    }
}