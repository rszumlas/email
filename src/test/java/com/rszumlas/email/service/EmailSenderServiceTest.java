package com.rszumlas.email.service;

import com.rszumlas.email.entity.Email;
import com.rszumlas.email.repository.EmailSenderRepository;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

class EmailSenderServiceTest {

    private EmailSenderService underTest;

    @Mock
    private JavaMailSender javaMailSender;

    @Mock
    private EmailSenderRepository emailSenderRepository;

    @Captor
    ArgumentCaptor<SimpleMailMessage> captor;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        underTest = new EmailSenderService(emailSenderRepository, javaMailSender);
    }

    @Test
    void itShouldSendMailToAll() {
        // Given
        List<Email> emails = Arrays.asList(
                new Email("user1@example.com"),
                new Email("user2@example.com")
        );

        // When
        when(emailSenderRepository.findAll()).thenReturn(emails);
        underTest.sendMailToAll("Test Subject", "Test Message");

        // Then
        verify(javaMailSender, times(2)).send(captor.capture());
        List<SimpleMailMessage> sentMessages = captor.getAllValues();
        assertThat(sentMessages)
                .flatExtracting(SimpleMailMessage::getTo)
                .containsExactlyInAnyOrder(new String[][]{{"user1@example.com"}, {"user2@example.com"}});
        assertThat(sentMessages).extracting(SimpleMailMessage::getSubject).containsOnly("Test Subject");
        assertThat(sentMessages).extracting(SimpleMailMessage::getText).containsOnly("Test Message");
    }

    @Test
    void itShouldCreateMailMessage() {
        // Given
        Email email = new Email("user1@example.com");
        String subject = "Test Subject";
        String message = "Test Message";

        // When
        SimpleMailMessage mailMessage = underTest.createMailMessage(email, subject, message);

        // Then
        assertThat(mailMessage.getTo()).isEqualTo(new String[]{email.getAddress()});
        assertThat(mailMessage.getSubject()).isEqualTo(subject);
        assertThat(mailMessage.getText()).isEqualTo(message);
    }
}