package com.rszumlas.email.controller;

import com.rszumlas.email.entity.Email;
import com.rszumlas.email.repository.EmailResourceRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class EmailResourceIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmailResourceRepository emailResourceRepository;

    @Test
    void shouldFindEmailByIdAndReturnOk() throws Exception {
        // Given
        Long id = 1L;

        // When
        when(emailResourceRepository.findById(id))
                .thenReturn(Optional.of(new Email(1L, "walter.white@example.com")));

        // Then
        mockMvc.perform(get("/api/v1/email-resources/{id}", id))
                .andExpect(status().isOk());
    }

    @Test
    void shouldNotFindEmailByIdAndReturnNotFound() throws Exception {
        // Given
        Long id = 1L;

        // When
        // Then
        mockMvc.perform(get("/api/v1/email-resources/{id}", id))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldSaveEmailAndReturnOk() throws Exception {
        // Given
        String address = "walter.white@example.com";

        // When
        // Then
        mockMvc.perform(post("/api/v1/email-resources/{address}", address))
                .andExpect(status().isOk());
    }

    @Test
    void shouldNotSaveEmailAndReturnBadRequest() throws Exception {
        // Given
        String address = "@example.com";

        // When
        // Then
        mockMvc.perform(post("/api/v1/email-resources/{address}", address))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldNotSaveEmailAndReturnConflict() throws Exception {
        // Given
        String address = "walter.white@example.com";

        // When
        when(emailResourceRepository.findByAddress(address))
                .thenReturn(Optional.of(new Email(1L, address)));

        // Then
        mockMvc.perform(post("/api/v1/email-resources/{address}", address))
                .andExpect(status().isConflict());
    }

    @Test
    void shouldUpdateEmailAndReturnOk() throws Exception {
        // Given
        Long id = 1L;
        String address = "walter.white@example.com";

        // When
        when(emailResourceRepository.findById(id))
                .thenReturn(Optional.of(new Email(id, address)));

        // Then
        mockMvc.perform(patch("/api/v1/email-resources/{id}/{address}", id, address))
                .andExpect(status().isOk());
    }

    @Test
    void shouldNotUpdateEmailAndReturnBadRequest() throws Exception {
        // Given
        Long id = 1L;
        String address = "@example.com";

        // When
        // Then
        mockMvc.perform(patch("/api/v1/email-resources/{id}/{address}", id, address))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldNotUpdateEmailAndReturnNotFound() throws Exception {
        // Given
        Long id = 1L;
        String address = "walter.white@example.com";

        // When
        when(emailResourceRepository.findById(id))
                .thenReturn(Optional.empty());

        // Then
        mockMvc.perform(patch("/api/v1/email-resources/{id}/{address}", id, address))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldDeleteEmailAndReturnOk() throws Exception {
        // Given
        Long id = 1L;
        String address = "walter.white@example.com";

        // When
        when(emailResourceRepository.findById(id))
                .thenReturn(Optional.of(new Email(id, address)));

        // Then
        mockMvc.perform(delete("/api/v1/email-resources/{id}", id))
                .andExpect(status().isOk());
    }

    @Test
    void shouldNotDeleteEmailAndReturnNotFound() throws Exception {
        // Given
        Long id = 1L;
        String address = "walter.white@example.com";

        // When
        when(emailResourceRepository.findById(id))
                .thenReturn(Optional.empty());

        // Then
        mockMvc.perform(delete("/api/v1/email-resources/{id}", id))
                .andExpect(status().isNotFound());
    }

}