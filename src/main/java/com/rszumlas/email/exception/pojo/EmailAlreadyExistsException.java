package com.rszumlas.email.exception.pojo;

public class EmailAlreadyExistsException extends RuntimeException {

    public EmailAlreadyExistsException(String address) {
        super("Email " + address + " already exists");
    }
}
