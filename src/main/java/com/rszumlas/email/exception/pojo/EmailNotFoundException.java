package com.rszumlas.email.exception.pojo;

public class EmailNotFoundException extends RuntimeException {

    public EmailNotFoundException(Long id) {
        super("Email with id " + id + " not found");
    }
}