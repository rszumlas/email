package com.rszumlas.email.exception.pojo;

public class AddressNotValidException extends RuntimeException {

    public AddressNotValidException(String address) {
        super("Address " + address + " is not valid");
    }
}
