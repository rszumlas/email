package com.rszumlas.email.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Email")
@Table(
        name = "email",
        uniqueConstraints = {
                @UniqueConstraint(
                        name = "unique_address_constraint",
                        columnNames = {"address"}
                )
        })
public class Email {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(
            name = "address",
            nullable = false
    )
    private String address;

    public Email(String address) {
        this.address = address;
    }
}
