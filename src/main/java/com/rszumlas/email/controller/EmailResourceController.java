package com.rszumlas.email.controller;

import com.rszumlas.email.entity.Email;
import com.rszumlas.email.service.EmailResourceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("api/v1/email-resources")
public class EmailResourceController {

    private final EmailResourceService emailResourceService;
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailResourceController.class);

    @Operation(summary = "Find email by ID", description = "Returns the email with the specified ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200", description = "Successful operation",
                    content = @Content(schema = @Schema(implementation = Email.class))),
            @ApiResponse(responseCode = "404", description = "Email not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    @GetMapping("{id}")
    public Email findEmailById(@PathVariable("id") Long id,
                               HttpServletRequest request) {
        LOGGER.info(request.getMethod() + " " + request.getRequestURL().toString());
        return emailResourceService.findEmailOrThrow(id);
    }

    @Operation(summary = "Save email", description = "Saves an email with the specified address")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "400", description = "Given address is not an email address"),
            @ApiResponse(responseCode = "409", description = "Given address already exists")
    })
    @PostMapping("{address}")
    public void saveEmail(@PathVariable("address") String address,
                          HttpServletRequest request) {
        LOGGER.info(request.getMethod() + " " + request.getRequestURL().toString());
        emailResourceService.saveEmail(address);
    }

    @Operation(summary = "Update email", description = "Updates the email with the specified ID to the new address")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "400", description = "Given address is not an email address"),
            @ApiResponse(responseCode = "404", description = "Email not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    @PatchMapping("{id}/{new-address}")
    public void updateEmail(@PathVariable("id") Long id,
                            @PathVariable("new-address") String newAddress,
                            HttpServletRequest request) {
        LOGGER.info(request.getMethod() + " " + request.getRequestURL().toString());
        emailResourceService.updateEmail(id, newAddress);
    }

    @Operation(summary = "Delete email", description = "Deletes the email with the specified ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "404", description = "Email not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    @DeleteMapping("{id}")
    public void deleteEmail(@PathVariable("id") Long id,
                            HttpServletRequest request) {
        LOGGER.info(request.getMethod() + " " + request.getRequestURL().toString());
        emailResourceService.deleteEmail(id);
    }

}
