package com.rszumlas.email.controller;

import com.rszumlas.email.service.EmailSenderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("api/v1/emails-sender")
public class EmailSenderController {

    private final EmailSenderService emailSenderService;
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailSenderController.class);

    @Operation(summary = "Sends mail message to all emails", description = "Sends mail message with given subject to all emails stored in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    @PostMapping("{subject}/{message}")
    public void sendMailToAll(@PathVariable("subject") String subject,
                              @PathVariable("message") String message,
                              HttpServletRequest request) {
        LOGGER.info(request.getMethod() + " " + request.getRequestURL().toString());
        emailSenderService.sendMailToAll(subject, message);
    }

}
