package com.rszumlas.email.service;

import com.rszumlas.email.entity.Email;
import com.rszumlas.email.exception.pojo.EmailAlreadyExistsException;
import com.rszumlas.email.exception.pojo.EmailNotFoundException;
import com.rszumlas.email.repository.EmailResourceRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.rszumlas.email.util.AddressValidator.convertAddressToLowerCase;

@AllArgsConstructor
@Service
public class EmailResourceService {

    private final EmailResourceRepository emailResourceRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailResourceService.class);

    public Email findEmailOrThrow(Long id) {
        Email email = emailResourceRepository.findById(id)
                .orElseThrow(() -> new EmailNotFoundException(id));
        LOGGER.debug("Successfully found email: " + email.toString());
        return email;
    }

    public void saveEmail(String address) {
        address = convertAddressToLowerCase(address);
        LOGGER.debug("Address converted and validated: " + address);

        emailResourceRepository.findByAddress(address)
                .ifPresent(email -> {
                    throw new EmailAlreadyExistsException(email.getAddress());
                });

        emailResourceRepository.save(new Email(address));
        LOGGER.debug("Successfully saved email");
    }

    public void updateEmail(Long id, String newAddress) {
        newAddress = convertAddressToLowerCase(newAddress);
        LOGGER.debug("Address converted and validated: " + newAddress);

        Email foundEmail = findEmailOrThrow(id);
        foundEmail.setAddress(newAddress);

        emailResourceRepository.save(foundEmail);
        LOGGER.debug("Successfully updated email");
    }

    public void deleteEmail(Long id) {
        findEmailOrThrow(id);
        emailResourceRepository.deleteById(id);
        LOGGER.debug("Successfully deleted email");
    }

}