package com.rszumlas.email.service;

import com.rszumlas.email.controller.EmailResourceController;
import com.rszumlas.email.entity.Email;
import com.rszumlas.email.repository.EmailSenderRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class EmailSenderService {

    private final EmailSenderRepository emailSenderRepository;
    private final JavaMailSender javaMailSender;
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailResourceController.class);

    public void sendMailToAll(String subject, String message) {
        Iterable<Email> emails = emailSenderRepository.findAll();
        for (Email e : emails) {
            SimpleMailMessage mailMessage = createMailMessage(e, subject, message);
            javaMailSender.send(mailMessage);
        }
        LOGGER.debug("Successfully sent mail messages to all emails");
    }

    protected SimpleMailMessage createMailMessage(Email email, String subject, String message) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email.getAddress());
        mailMessage.setSubject(subject);
        mailMessage.setText(message);
        return mailMessage;
    }

}
