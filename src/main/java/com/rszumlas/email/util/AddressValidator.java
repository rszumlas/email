package com.rszumlas.email.util;

import com.rszumlas.email.exception.pojo.AddressNotValidException;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class AddressValidator {

    public static String convertAddressToLowerCase(String address) {
        try {
            InternetAddress internetAddress = new InternetAddress(address);
            internetAddress.validate();
            String lowerCaseAddress = internetAddress.getAddress().toLowerCase();
            return lowerCaseAddress;
        } catch (AddressException ex) {
            throw new AddressNotValidException(address);
        }
    }

}
