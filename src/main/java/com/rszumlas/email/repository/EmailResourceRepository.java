package com.rszumlas.email.repository;

import com.rszumlas.email.entity.Email;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface EmailResourceRepository extends CrudRepository<Email, Long> {

    boolean existsByAddress(String address);

    Optional<Email> findByAddress(String address);
}
