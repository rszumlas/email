package com.rszumlas.email.repository;

import com.rszumlas.email.entity.Email;
import org.springframework.data.repository.CrudRepository;

public interface EmailSenderRepository extends CrudRepository<Email, Long> {
}
